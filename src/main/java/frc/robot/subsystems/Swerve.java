package frc.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.TeleopSwerve;
import frc.robot.swerve.SwerveModule;
import frc.robot.swerve.motors.angle.TalonFXSwerveAngleMotor;
import frc.robot.swerve.motors.drive.TalonFXSwerveDriveMotor;

public class Swerve extends SubsystemBase {
    public SwerveModule[] modules;
    public AHRS gyro;

    public SwerveDrivePoseEstimator poseEstimator;
    private SlewRateLimiter limiterx, limitery;

    private boolean braking;

    public Swerve() {
        this.braking = false;
        this.gyro = new AHRS(Port.kMXP);

        this.limiterx = new SlewRateLimiter(6);
        this.limitery = new SlewRateLimiter(6);

        this.modules = new SwerveModule[] {
                new SwerveModule(0, TalonFXSwerveAngleMotor::new, TalonFXSwerveDriveMotor::new,
                        Constants.Swerve.Mod0.MODULE),
                new SwerveModule(1, TalonFXSwerveAngleMotor::new, TalonFXSwerveDriveMotor::new,
                        Constants.Swerve.Mod1.MODULE),
                new SwerveModule(2, TalonFXSwerveAngleMotor::new, TalonFXSwerveDriveMotor::new,
                        Constants.Swerve.Mod2.MODULE),
                new SwerveModule(3, TalonFXSwerveAngleMotor::new, TalonFXSwerveDriveMotor::new,
                        Constants.Swerve.Mod3.MODULE)
        };

        this.poseEstimator = new SwerveDrivePoseEstimator(Constants.Swerve.swerveKinematics, getYaw(),
                getModulePositions(),
                new Pose2d());

        resetModulesToAbsolute();
        zeroAll();
    }

    /**
     * Drives the robot in swerve mode with no rate limiting
     * 
     * @param translation Direction and speed to go in. When in closed loop,
     *                    the speed is in meters per second.
     * @param rotation    Rotational velocity of the robot
     *                    in radians per second.
     * @param isOpenLoop  Whether to drive the robot in open loop or closed loop.
     *                    Open loop uses percent output for speed while cloesd loop
     *                    uses exact
     *                    velocities and PIDs.
     * @see TeleopSwerve
     */
    public void drive(Translation2d translation, double rotation, boolean isOpenLoop) {
        drive(translation, rotation, isOpenLoop, false);
    }

    /**
     * Drives the robot in swerve mode
     * 
     * @param translation   Direction and speed to go in. When in closed loop,
     *                      the speed is in meters per second.
     * @param rotation      Rotational velocity of the robot
     *                      in radians per second.
     * @param isOpenLoop    Whether to drive the robot in open loop or closed loop.
     *                      Open loop uses percent output for speed while cloesd
     *                      loop uses exact PID
     *                      controlled velocities.
     * @param isRateLimited Whether to limit how fast the robot can change
     *                      directions.
     * @see TeleopSwerve
     */
    public void drive(Translation2d translation, double rotation, boolean isOpenLoop, boolean isRateLimited) {
        if (braking)
            return;
        SwerveModuleState[] swerveModuleStates = Constants.Swerve.swerveKinematics.toSwerveModuleStates(
                ChassisSpeeds.fromFieldRelativeSpeeds(
                        isRateLimited ? limiterx.calculate(translation.getX()) : translation.getX(),
                        isRateLimited ? limitery.calculate(translation.getY()) : translation.getY(),
                        rotation,
                        getYaw()));

        SwerveDriveKinematics.desaturateWheelSpeeds(swerveModuleStates, Constants.Swerve.MAX_SPEED);

        for (SwerveModule mod : modules) {
            mod.setDesiredState(swerveModuleStates[mod.moduleNumber], isOpenLoop);
        }
    }

    /**
     * Equivalent to an {@link InstantCommand} that drives the robot
     * 
     * @param translation Direction and speed to go in.
     * @param rotation    Rotation velocity of the
     *                    robot in radians per second.
     * @return The drive command
     */
    public Command driveCommand(Translation2d translation, double rotation) {
        return this.runOnce(() -> drive(translation, rotation, false));
    }

    /**
     * Sets the states of each swerve module.
     * This is another way of driving the robot that uses
     * precise velocities.
     * 
     * @param desiredStates States of each swerve module
     */
    public void setModuleStates(SwerveModuleState[] desiredStates) {
        SwerveDriveKinematics.desaturateWheelSpeeds(desiredStates, Constants.Swerve.MAX_SPEED);
        for (SwerveModule mod : modules) {
            mod.setDesiredState(desiredStates[mod.moduleNumber], false);
        }
    }

    /**
     * Gets the estimated position of the robot
     * 
     * @return Current estimated position of the robot
     */
    public Pose2d getPose() {
        return poseEstimator.getEstimatedPosition();
    }

    /**
     * Get the current braking state of the robot
     * 
     * @return The current braking state
     */
    public boolean isBraking() {
        return braking;
    }

    /**
     * Toggles the braking state of the robot.
     * <p>
     * Braking is when all the robot's wheel are angled 45 degrees
     * to resist any translational motion.
     */
    public void toggleBrake() {
        setBraking(!braking);
    }

    /**
     * Sets the braking state of the robot.
     * 
     * @param braking Whether the robot should be braking
     *                <p>
     *                Braking is when all the robot's wheel are angled 45 degrees
     *                to resist any translational motion.
     */
    public void setBraking(boolean braking) {
        this.braking = braking;
        if (braking) {
            setModuleStates(new SwerveModuleState[] {
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(0)),
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(0)),
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(0)),
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(0))
            });
            setModuleStates(new SwerveModuleState[] {
                    new SwerveModuleState(0, Rotation2d.fromDegrees(0)),
                    new SwerveModuleState(0, Rotation2d.fromDegrees(0)),
                    new SwerveModuleState(0, Rotation2d.fromDegrees(0)),
                    new SwerveModuleState(0, Rotation2d.fromDegrees(0))
            });
        } else {
            setModuleStates(new SwerveModuleState[] {
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(-45)),
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(45)),
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(45)),
                    new SwerveModuleState(Constants.Swerve.MAX_SPEED * 0.011, Rotation2d.fromDegrees(-45))
            });
            setModuleStates(new SwerveModuleState[] {
                    new SwerveModuleState(0, Rotation2d.fromDegrees(-45)),
                    new SwerveModuleState(0, Rotation2d.fromDegrees(45)),
                    new SwerveModuleState(0, Rotation2d.fromDegrees(45)),
                    new SwerveModuleState(0, Rotation2d.fromDegrees(-45))
            });
        }
        SmartDashboard.putBoolean("Is Braking", braking);
    }

    /**
     * Gets the state of all the swerve modules
     * <p>
     * States are represented by {@link SwerveModuleState}, which
     * is a representation of the linear velocity and angle of each
     * swerve module wheel.
     * 
     * @return Array of states
     * @see SwerveModuleState
     */
    public SwerveModuleState[] getModuleStates() {
        SwerveModuleState[] states = new SwerveModuleState[4];
        for (SwerveModule mod : modules) {
            states[mod.moduleNumber] = mod.getState();
        }
        return states;
    }

    /**
     * Get the positions of all of the swerve modules
     * <p>
     * Positions are represented by {@link SwerveModulePosition}, which
     * stores the measured displacement on the motor encoders and the
     * measured angle of the wheel.
     * 
     * @return Array of positions
     * @see SwerveModulePosition
     */
    public SwerveModulePosition[] getModulePositions() {
        SwerveModulePosition[] positions = new SwerveModulePosition[4];
        for (SwerveModule mod : modules) {
            positions[mod.moduleNumber] = mod.getPosition();
        }
        return positions;
    }

    /**
     * Sets the current yaw of the robot as the "zero"
     */
    public void zeroYaw() {
        gyro.zeroYaw();
    }

    /**
     * Resets the estimated robot position
     * 
     * @param pose The pose to set the current position to
     */
    public void resetOdometry(Pose2d pose) {
        poseEstimator.resetPosition(getYaw(), getModulePositions(), pose);
    }

    /**
     * Sets the current yaw of the robot as the "zero" and
     * resets the estimated robot position
     */
    public void zeroAll() {
        zeroYaw();
        resetOdometry(new Pose2d(0, 0, new Rotation2d()));
    }

    /**
     * Gets the yaw of the robot in degrees
     * 
     * @return Yaw in degrees
     */
    public double getYawDegrees() {
        return Math.abs(gyro.getYaw() - 180) - 180;
    }

    /**
     * Gets the yaw of the robot
     * 
     * @return Rotation2d of the robot yaw
     */
    public Rotation2d getYaw() {
        // should be ccw+
        return Rotation2d.fromDegrees(getYawDegrees());
    }

    /**
     * Resets the relative encoders in the motors to zero
     */
    public void resetModulesToAbsolute() {
        for (SwerveModule mod : modules) {
            mod.resetToAbsolute();
        }
    }

    @Override
    public void periodic() {
        poseEstimator.update(getYaw(), getModulePositions());
        SmartDashboard.putNumber("Roll", gyro.getRoll());
        SmartDashboard.putNumber("Heading", getYaw().getDegrees());
        for (SwerveModule mod : modules) {
            SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Cancoder", mod.getCanCoder().getDegrees());
            SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Integrated", mod.getPosition().angle.getDegrees());
            SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Velocity", mod.getState().speedMetersPerSecond);
        }
    }
}