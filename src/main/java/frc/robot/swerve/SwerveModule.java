package frc.robot.swerve;

import java.util.function.Function;

import com.ctre.phoenix6.hardware.CANcoder;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import frc.robot.Constants;
import frc.robot.swerve.motors.angle.SwerveAngleMotor;
import frc.robot.swerve.motors.drive.SwerveDriveMotor;
import frc.robot.util.Conversions;
import frc.robot.util.SwerveModuleConstants;

public class SwerveModule {
    public int moduleNumber;
    private Rotation2d angleOffset;
    private Rotation2d lastAngle;

    private SwerveAngleMotor angleMotor;
    private SwerveDriveMotor driveMotor;

    private CANcoder angleEncoder;

    private SimpleMotorFeedforward feedforward;

    /**
     * Creates a {@link SwerveModule}
     * 
     * @param moduleNumber    Swerve module number
     * @param angleMotor      A function that creates a swerve angle motor
     * @param driveMotor      A function that creates a swerve drive motor
     * @param moduleConstants Swerve constants for module
     */
    public SwerveModule(int moduleNumber, Function<Integer, SwerveAngleMotor> angleMotor,
            Function<Integer, SwerveDriveMotor> driveMotor, SwerveModuleConstants moduleConstants) {
        this.moduleNumber = moduleNumber;
        this.angleOffset = moduleConstants.angleOffset;

        /* Angle Encoder Config */
        angleEncoder = new CANcoder(moduleConstants.cancoderID);
        configAngleEncoder();

        /* Drive Motor Config */
        this.driveMotor = driveMotor.apply(moduleConstants.driveMotorID);
        this.driveMotor.config();

        /* Angle Motor Config */
        this.angleMotor = angleMotor.apply(moduleConstants.angleMotorID);
        this.angleMotor.config();
        resetToAbsolute();

        feedforward = new SimpleMotorFeedforward(Constants.Swerve.DRIVE_S, Constants.Swerve.DRIVE_V,
                Constants.Swerve.DRIVE_A);

        lastAngle = getState().angle;
    }

    private void configAngleEncoder() {
        angleEncoder.getConfigurator().apply(Constants.Swerve.CTRE_CONFIGS.swerveCanCoderConfig);
    }

    /**
     * Resets the motor relative encoder to the measured absolute position from the
     * CANcoder
     */
    public void resetToAbsolute() {
        double absolutePosition = getCanCoder().getRotations() - angleOffset.getRotations();
        angleMotor.setSensorPosition(absolutePosition);
    }

    /**
     * Set only the angle of the module
     * 
     * @param desiredState Drives the module to a {@link SwerveModuleState}
     */
    public void setAngle(SwerveModuleState desiredState) {
        // Prevent rotating module if speed is less then 1%. Prevents Jittering.
        Rotation2d angle = (Math.abs(desiredState.speedMetersPerSecond) <= (Constants.Swerve.MAX_SPEED * 0.01))
                ? lastAngle
                : desiredState.angle;
        angleMotor.setPosition(angle.getRotations());
        lastAngle = angle;
    }

    /**
     * Set only the velocity to drive the module at
     * 
     * @param desiredState Drives the module to a {@link SwerveModuleState}
     * @param isOpenLoop   Set to true to run this without PID control
     */
    public void setSpeed(SwerveModuleState desiredState, boolean isOpenLoop) {
        if (isOpenLoop) {
            double percentOutput = desiredState.speedMetersPerSecond / Constants.Swerve.MAX_SPEED;
            driveMotor.set(percentOutput);
        } else {
            driveMotor.setLinearVelocity(desiredState.speedMetersPerSecond,
                    feedforward.calculate(desiredState.speedMetersPerSecond));
        }
    }

    /**
     * Sets the velocity and angle to drive the module at
     * 
     * @param desiredState Drives the module to a {@link SwerveModuleState}
     * @param isOpenLoop   Set to true to run this without PID control
     */
    public void setDesiredState(SwerveModuleState desiredState, boolean isOpenLoop) {
        /*
         * This is a custom optimize function, since default WPILib optimize assumes
         * continuous controller which CTRE and Rev onboard is not
         */
        desiredState = SwerveModule.optimize(desiredState, getState().angle);
        setAngle(desiredState);
        setSpeed(desiredState, isOpenLoop);
    }

    /**
     * Gets the relative position of the angle as measured by the motor encoder
     * This is originally set to be equal to the absolute position
     * 
     * @return Relative angle as Rotation2d
     * @see Rotation2d
     */
    public Rotation2d getAngle() {
        return Rotation2d.fromDegrees(Conversions.rotationsToDegrees(angleMotor.getPosition()));
    }

    /**
     * Gets the absolute position of the angle as measured by the CANcoder
     * 
     * @return Absolute angle as Rotation2d
     * @see Rotation2d
     */
    public Rotation2d getCanCoder() {
        return Rotation2d.fromRotations(angleEncoder.getAbsolutePosition().getValue());
    }

    /**
     * Gets the linear velocity of the drive motor
     * 
     * @return State with angle and velcity
     * @see SwerveModuleState
     */
    public SwerveModuleState getState() {
        return new SwerveModuleState(
                driveMotor.getLinearVelocity(),
                getAngle());
    }

    /**
     * Gets the position and displacement of the module
     * 
     * @return Position with angle and displacement
     * @see SwerveModulePosition
     */
    public SwerveModulePosition getPosition() {
        return new SwerveModulePosition(
                driveMotor.getDisplacement(),
                getAngle());
    }

    /**
     * Gets the abstracted angle motor
     * 
     * @return Returns the {@link SwerveAngleMotor}
     */
    public SwerveAngleMotor getAngleMotor() {
        return angleMotor;
    }

    /**
     * Gets the abstracted drive motor
     * 
     * @return Returns the {@link SwerveDriveMotor}
     */
    public SwerveDriveMotor getDriveMotor() {
        return driveMotor;
    }

    /**
     * Minimize the change in heading the desired swerve module state would require
     * by potentially
     * reversing the direction the wheel spins. Customized from WPILib's version to
     * include placing
     * in appropriate scope for CTRE onboard control.
     *
     * @param desiredState The desired state.
     * @param currentAngle The current module angle.
     */
    public static SwerveModuleState optimize(SwerveModuleState desiredState, Rotation2d currentAngle) {
        double targetAngle = placeInAppropriate0To360Scope(currentAngle.getDegrees(), desiredState.angle.getDegrees());
        double targetSpeed = desiredState.speedMetersPerSecond;
        double delta = targetAngle - currentAngle.getDegrees();
        if (Math.abs(delta) > 90) {
            targetSpeed = -targetSpeed;
            targetAngle = delta > 90 ? (targetAngle -= 180) : (targetAngle += 180);
        }
        return new SwerveModuleState(targetSpeed, Rotation2d.fromDegrees(targetAngle));
    }

    /**
     * @param scopeReference Current Angle
     * @param newAngle       Target Angle
     * @return Closest angle within scope
     */
    private static double placeInAppropriate0To360Scope(double scopeReference, double newAngle) {
        double lowerBound;
        double upperBound;
        double lowerOffset = scopeReference % 360;
        if (lowerOffset >= 0) {
            lowerBound = scopeReference - lowerOffset;
            upperBound = scopeReference + (360 - lowerOffset);
        } else {
            upperBound = scopeReference - lowerOffset;
            lowerBound = scopeReference - (360 + lowerOffset);
        }
        while (newAngle < lowerBound) {
            newAngle += 360;
        }
        while (newAngle > upperBound) {
            newAngle -= 360;
        }
        if (newAngle - scopeReference > 180) {
            newAngle -= 360;
        } else if (newAngle - scopeReference < -180) {
            newAngle += 360;
        }
        return newAngle;
    }
}