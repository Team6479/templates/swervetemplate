// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.angle;

import com.ctre.phoenix6.controls.PositionVoltage;
import com.ctre.phoenix6.hardware.TalonFX;

import frc.robot.Constants;

public class TalonFXSwerveAngleMotor extends SwerveAngleMotor {
    private final TalonFX motor;

    private final PositionVoltage positionControl;

    public TalonFXSwerveAngleMotor(int canId) {
        this.motor = new TalonFX(canId);
        this.positionControl = new PositionVoltage(0);
    }

    @Override
    public void set(double speed) {
        motor.set(speed);
    }

    @Override
    public void setPosition(double rotations) {
        motor.setControl(positionControl.withPosition(rotations));
    }

    @Override
    public double getPosition() {
        return motor.getPosition().getValue();
    }

    @Override
    public void setSensorPosition(double rotations) {
        motor.setPosition(rotations);
    }

    @Override
    public void config() {
        motor.getConfigurator().apply(Constants.Swerve.CTRE_CONFIGS.swerveAngleConfig);
        motor.setInverted(Constants.Swerve.INVERT_ANGLE_MOTORS);
        motor.setNeutralMode(Constants.Swerve.ANGLE_NEUTRAL_MODE);
    }
    
    @Override
    public TalonFX getMotor() {
        return motor;
    }
}
