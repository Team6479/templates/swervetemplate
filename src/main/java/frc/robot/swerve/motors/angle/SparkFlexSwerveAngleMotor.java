// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.angle;

import com.revrobotics.CANSparkFlex;
import com.revrobotics.CANSparkLowLevel.MotorType;

public class SparkFlexSwerveAngleMotor extends SparkSwerveAngleMotor {
    public SparkFlexSwerveAngleMotor(int canId) {
        super(new CANSparkFlex(canId, MotorType.kBrushless));
    }
}
