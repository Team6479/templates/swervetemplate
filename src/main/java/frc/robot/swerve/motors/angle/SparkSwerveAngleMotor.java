// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.angle;

import com.revrobotics.CANSparkBase;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import com.revrobotics.CANSparkBase.ControlType;

import frc.robot.Constants;

public abstract class SparkSwerveAngleMotor extends SwerveAngleMotor {
    private final CANSparkBase motor;

    private final SparkPIDController controller;
    private final RelativeEncoder encoder;

    public SparkSwerveAngleMotor(CANSparkBase motor) {
        this.motor = motor;

        this.controller = motor.getPIDController();
        this.encoder = motor.getEncoder();
    }

    @Override
    public void set(double speed) {
        motor.set(speed);
    }

    @Override
    public void setPosition(double rotations) {
        controller.setReference(rotations, ControlType.kPosition);
    }

    @Override
    public void setSensorPosition(double rotations) {
        encoder.setPosition(rotations);
    }

    @Override
    public double getPosition() {
        return encoder.getPosition();
    }

    @Override
    public void config() {
        motor.restoreFactoryDefaults();
        if (Constants.Swerve.ANGLE_ENABLE_CURRENT_LIMIT) {
            motor.setSmartCurrentLimit(Constants.Swerve.ANGLE_CONTINUOUS_CURRENT_LIMIT);
        }

        controller.setP(Constants.Swerve.ANGLE_P);
        controller.setI(Constants.Swerve.ANGLE_I);
        controller.setD(Constants.Swerve.ANGLE_D);
        controller.setFF(Constants.Swerve.ANGLE_F);

        encoder.setPositionConversionFactor(1 / Constants.Swerve.ANGLE_GEAR_RATIO);
        encoder.setVelocityConversionFactor(1 / Constants.Swerve.ANGLE_GEAR_RATIO);
        
        motor.setInverted(Constants.Swerve.INVERT_ANGLE_MOTORS);
        motor.setIdleMode(Constants.Swerve.ANGLE_IDLE_MODE);
    }

    @Override
    public CANSparkBase getMotor() {
        return motor;
    }

}
