// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.angle;

import com.ctre.phoenix6.hardware.TalonFX;
import com.revrobotics.CANSparkBase;

import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.Units;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public abstract class SwerveAngleMotor {
    public SwerveAngleMotor() {
    }

    /**
     * Runs a motor with percent ouput/duty cycle
     * 
     * @param speed Percent output [-1, 1]
     */
    public abstract void set(double speed);

    /**
     * Sets the target position of the mechanism in rotations
     * 
     * @param rotations Target position in rotations
     */
    public abstract void setPosition(double rotations);

    /**
     * Sets the target position of the mechanism
     * 
     * @param position Target position
     */
    public void setPosition(Measure<Angle> position) {
        setPosition(position.in(Units.Rotations));
    }

    /**
     * Sets the position that the sensor reads
     * 
     * @param rotations Position in rotations
     */
    public abstract void setSensorPosition(double rotations);

    /**
     * Sets the position that the sensor reads
     * 
     * @param angle Motor sensor position angle
     */
    public void setSensorPosition(Measure<Angle> angle) {
        setSensorPosition(angle.in(Units.Rotations));
    }

    /**
     * Get the position of the mechanism in rotations
     * 
     * @return Rotations
     */
    public abstract double getPosition();

    /**
     * Get the position of the mechanism
     * 
     * @return Position of the mechanism
     */
    public Measure<Angle> getPositionMeasure() {
        return Units.Rotations.of(getPosition());
    }

    /**
     * Run once to setup motor and sensor configs
     */
    public abstract void config();

    /**
     * Get the underlying motor controller
     * 
     * @return Motor controller like {@link TalonFX} or {@link CANSparkBase}
     */
    public abstract MotorController getMotor();
}
