// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.angle;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

public class SparkMaxSwerveAngleMotor extends SparkSwerveAngleMotor {
    public SparkMaxSwerveAngleMotor(int canId) {
        super(new CANSparkMax(canId, MotorType.kBrushless));
    }
}
