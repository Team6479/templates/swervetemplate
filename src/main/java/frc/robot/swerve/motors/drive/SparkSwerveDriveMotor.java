package frc.robot.swerve.motors.drive;

import com.revrobotics.CANSparkBase;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import com.revrobotics.CANSparkBase.ControlType;

import frc.robot.Constants;
import frc.robot.util.Conversions;

public abstract class SparkSwerveDriveMotor extends SwerveDriveMotor {
    private final CANSparkBase motor;

    private final SparkPIDController controller;
    private final RelativeEncoder encoder;

    public SparkSwerveDriveMotor(CANSparkBase motor) {
        this.motor = motor;

        this.controller = motor.getPIDController();
        this.encoder = motor.getEncoder();
    }

    @Override
    public void set(double speed) {
        motor.set(speed);
    }

    @Override
    public void setVelocity(double rpm, double feedforward) {
        controller.setReference(rpm, ControlType.kVelocity, 0, feedforward);
    }

    @Override
    public void setLinearVelocity(double mps, double feedfoward) {
        controller.setReference(
            Conversions.MPSToRPM(mps, Constants.Swerve.WHEEL_CIRCUMFERENCE), 
            ControlType.kVelocity,
            0,
            feedfoward
        );
    }

    @Override
    public double getVelocity() {
        return encoder.getVelocity();
    }

    @Override
    public double getLinearVelocity() {
        return Conversions.RPMToMPS(encoder.getVelocity(), Constants.Swerve.WHEEL_CIRCUMFERENCE);
    }

    @Override
    public double getPosition() {
        // TODO: this could possibly be wrong because encoder.getPosition() might not return mechanism rotations
        // The gear ratio might have to be refactored in
        return encoder.getPosition();
    }

    @Override
    public double getDisplacement() {
        // TODO: this could possibly be wrong because encoder.getPosition() might not return mechanism rotations
        // The gear ratio might have to be refactored in
        return Conversions.rotationsToMeters(encoder.getPosition(), Constants.Swerve.WHEEL_CIRCUMFERENCE);
    }

    @Override
    public void config() {
        motor.setSmartCurrentLimit(Constants.Swerve.DRIVE_CONTINUOUS_CURRENT_LIMIT);
        
        controller.setP(Constants.Swerve.DRIVE_P);
        controller.setI(Constants.Swerve.DRIVE_I);
        controller.setD(Constants.Swerve.DRIVE_D);
        controller.setFF(Constants.Swerve.DRIVE_F);

        encoder.setPositionConversionFactor(1 / Constants.Swerve.DRIVE_GEAR_RATIO);
        encoder.setVelocityConversionFactor(1 / Constants.Swerve.DRIVE_GEAR_RATIO);

        motor.setOpenLoopRampRate(Constants.Swerve.OPEN_LOOP_RAMP);
        motor.setClosedLoopRampRate(Constants.Swerve.CLOSED_LOOP_RAMP);

        motor.setInverted(Constants.Swerve.INVERT_DRIVE_MOTORS);
        motor.setIdleMode(Constants.Swerve.DRIVE_IDLE_MODE);
        encoder.setPosition(0);
    }

    @Override
    public CANSparkBase getMotor() {
        return motor;
    }
    
}
