// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.drive;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

public class SparkMaxSwerveDriveMotor extends SparkSwerveDriveMotor {
    public SparkMaxSwerveDriveMotor(int canId) {
        super(new CANSparkMax(canId, MotorType.kBrushless));
    }
}
