// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.drive;

import com.ctre.phoenix6.hardware.TalonFX;
import com.revrobotics.CANSparkBase;

import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Measure;
import edu.wpi.first.units.Units;
import edu.wpi.first.units.Velocity;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public abstract class SwerveDriveMotor {
    public SwerveDriveMotor() {
    }

    /**
     * Runs a motor with percent ouput/duty cycle
     * 
     * @param speed Percent output [-1, 1]
     */
    public abstract void set(double speed);

    /**
     * Sets the target velocity of the mechanism in RPM
     * 
     * @param rpm Target velocity in RPM
     */
    public void setVelocity(double rpm) {
        setVelocity(rpm, 0);
    }

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param velocity Target velocity
     */
    public void setVelocity(Measure<Velocity<Angle>> velocity) {
        setVelocity(velocity, 0);
    }

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param velocity   Target velocity
     * @param feedfoward Arbitrary feedfoward to add
     */
    public void setVelocity(Measure<Velocity<Angle>> velocity, double feedforward) {
        setVelocity(velocity.in(Units.RPM), feedforward);
    }

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param rpm        Target velocity in RPM
     * @param feedfoward Arbitrary feedfoward to add
     */
    public abstract void setVelocity(double rpm, double feedforward);

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param mps Target velocity in meters per second
     */
    public void setLinearVelocity(double mps) {
        setLinearVelocity(mps, 0);
    }

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param mps Target velocity
     */
    public void setLinearVelocity(Measure<Velocity<Distance>> velocity) {
        setLinearVelocity(velocity, 0);
    }

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param mps        Target velocity
     * @param feedfoward Arbitrary feedfoward to add
     */
    public void setLinearVelocity(Measure<Velocity<Distance>> velocity, double feedforward) {
        setLinearVelocity(velocity.in(Units.MetersPerSecond), feedforward);
    }

    /**
     * Sets the target velocity of the mechanism
     * 
     * @param mps        Target velocity in meters per second
     * @param feedfoward Arbitrary feedfoward to add
     */
    public abstract void setLinearVelocity(double mps, double feedforward);

    /**
     * Get the velocity of the mechanism in RPM
     * 
     * @return Current velocity in RPM
     */
    public abstract double getVelocity();

    /**
     * Get the linearized velocity of the mechanism in meters per second
     * 
     * @return Current velocity in meters per second
     */
    public abstract double getLinearVelocity();

    /**
     * Get the position of the mechanism in rotations
     * 
     * @return Rotations
     */
    public abstract double getPosition();

    /**
     * Get the displacement from the number of rotations of the wheel
     * 
     * @return Meters
     */
    public abstract double getDisplacement();

    /**
     * Get the velocity of the mechanism
     * 
     * @return Current velocity
     */
    public Measure<Velocity<Angle>> getVelocityMeasure() {
        return Units.RPM.of(getVelocity());
    }

    /**
     * Get the linearized velocity of the mechanism
     * 
     * @return Current linearized velocity
     */
    public Measure<Velocity<Distance>> getLinearVelocityMeasure() {
        return Units.MetersPerSecond.of(getLinearVelocity());
    }

    /**
     * Get the position of the mechanism
     * 
     * @return Wheel position
     */
    public Measure<Angle> getPositionMeasure() {
        return Units.Rotations.of(getPosition());
    }

    /**
     * Get the displacement from the number of rotations of the wheel
     * 
     * @return Total wheel displacement
     */
    public Measure<Distance> getDisplacementMeasure() {
        return Units.Meters.of(getDisplacement());
    }

    /**
     * Run once to setup motor and sensor configs
     */
    public abstract void config();

    /**
     * Get the underlying motor controller
     * 
     * @return Motor controller like {@link TalonFX} or {@link CANSparkBase}
     */
    public abstract MotorController getMotor();
}
